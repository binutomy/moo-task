import {
    BASKET_LOADED, ADD_TO_BASKET, REMOVE_FROM_BASKET,
} from '../actions/basket';

function basketReducer(state={ items: [] }, action) {
    switch(action.type) {
        case BASKET_LOADED:
            return {
                ...state, 
                id: action.cartId,
                items: action.cartItems,
            }
        case ADD_TO_BASKET:
            return {
                ...state,
                id: action.productId,
                items: action.items
            }    
        case REMOVE_FROM_BASKET:
            return {
                ...state,
                id: action.productId,
                items: action.items
            }
        default:
            return state;
    }

}

export default basketReducer;
