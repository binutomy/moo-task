import React from 'react';

const ShoppingBasket = ({ products, removeFromBasket }) => (
    products &&
    <div className="mr-4">
        <h2>Basket</h2>
        <ul className="list-group">
            {
                products.map(product =>{                    
                    return (
                        <li className="list-group-item d-flex justify-content-between" key={product.name}>
                            <span>{product.name}</span>
                            <button className="btn btn-outline-primary btn-sm" type="button" onClick={() => removeFromBasket(product.id)}>Remove</button>
                        </li>
                    )
                }) 
            }
        </ul>
    </div> 
);

export default ShoppingBasket;
