export const BASKET_LOADING = 'BASKET_LOADING';
export const BASKET_LOADED = 'BASKET_LOADED';
export const ADD_TO_BASKET = 'ADD_TO_BASKET';
export const REMOVE_FROM_BASKET = 'REMOVE_FROM_BASKET';

export function loadBasket() {
  return async (dispatch, _, config) => {
    dispatch({ type: BASKET_LOADING });

    const response = await fetch(config.cartApi, {
      method: 'POST',
      body: JSON.stringify({})
    });
    const basket = await response.json();

    dispatch({
      type: BASKET_LOADED,
      ...basket
    });
  };
}

export function removeFromBasket(productId, items) {
  return {
    type: REMOVE_FROM_BASKET,
    productId,
    items
  };
}

export function addToBasket(productId, items) {
  return async (dispatch, _, config) => {
    dispatch({ type: BASKET_LOADING });

    // const response = await fetch(`${config.cartApi}/${productId}`, {
    //   method: 'POST',
    //   body: JSON.stringify({})
    // });
    // const basket = await response.json();

    dispatch({
      type: ADD_TO_BASKET,
      productId,
      items
    });
  };
}
